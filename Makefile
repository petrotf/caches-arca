main : main.c
	gcc -g -O3 $< -o $@
run :   # main nb de set, nb de mots par ligne, nb de voies
	./main 32 4 2 < insns-fetch | awk '/k-/{print $$2, $$6}' | sed 's/,//' > file.txt
clean : 
	rm main
