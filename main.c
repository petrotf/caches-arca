/*
 *  Simulateur de cache monoprocesseur fait vite fait sur le gaz
 *  pour le cours d'Archi 2A de l'Ensimag
 *  Copyright (C) 2020 Frédéric Pétrot <frederic.petrot@univ-grenoble-alpes.fr>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include<stdint.h>
#include<stdbool.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>


/* Quelques types pour modéliser un cache d'instructions */
typedef uint32_t cpu_addr;

/* Inutile d'avoir les instructions pour la simulation, seules les adresses comptent en fait ! */
typedef struct cache_line {
   int32_t age;
   uint32_t v;
   uint32_t tag;
} cache_line;

/*\
 * Arrondi par exces de log2, pour calculer le nombre de bits qu'il faut pour représenter un nombre
 * Implem de tueur à l'aide de clz (count leading zeros), difficile de faire mieux sur x86,
 * Merci Hacker's Delight !
\*/
uint32_t clog2(uint32_t x)
{
   return 31 - __builtin_clz(x << 1 - 1);
}

void usage(char *prog)
{
   fprintf(stderr, "Usage : %s [-v] <nb d'ensembles> <nb de mots par ligne> <nb de voies>\n"
                   "Tous les nombres doivent être présents et être des puissances entières de 2\n"
                   "-v fait une sortie par addresse lue, sinon statistiques toutes les 1000 instructions\n",
                   prog);
   exit(1);
}

int main(int argc, char *argv[])
{
   bool verbose = 0;

   if (argc < 4 || argc > 5)
      usage(argv[0]);
   else
      if (argc == 5)
         if (!strcmp(argv[1], "-v"))
            verbose = 1;
         else 
            usage(argv[0]);

   uint32_t e   = strtol(argv[verbose + 1], NULL, 0);
   uint32_t mpl = strtol(argv[verbose + 2], NULL, 0);
   uint32_t v   = strtol(argv[verbose + 3], NULL, 0);

   if ((1 << clog2(e)) != e || (1 << clog2(mpl)) != mpl || (1 << clog2(v)) != v)
      usage(argv[0]);

   printf("Géométrie du cache : nb lignes = %d, nb de mots par ligne %d, nb de voies = %d, taille en octets = %d\n", e, mpl, v, e * mpl * 4 * v);

   /* On utilise la terminologie anglophone, il faut la connaître */
   uint32_t offset_b = clog2(mpl);                 // Nb de bits de l'offset
   uint32_t sets_b   = clog2(e);                   // Nb de bits du nombre d'ensembles
   uint32_t ways_b   = clog2(v);                   // Nb de bits des voies
   uint32_t tag_b    = 32 - sets_b - offset_b - 2; // Nb de bits du tag

   cache_line cache[v][e];  /* Merci c99! */

   /* Création et initialisation du cache, pas d'instructions */
   for (int i = 0; i < v; i++) {
      for (int j = 0; j < e; j++) {
         cache[i][j].age  = 0;
         cache[i][j].v    = 0;
         cache[i][j].tag  = 0;
      }
   }

   char buffer[BUFSIZ];
   /* Pour les statistiques */
   uint32_t insns = 0;
   uint32_t hits[v];
   for (int i = 0; i < v; i++)
      hits[i] = 0;

   while (fgets(buffer, BUFSIZ, stdin)) {
      cpu_addr insn_addr = strtol(buffer, NULL, 0); 

      insns++; /* On compte le nombre d'instructions */

      uint32_t offset = (insn_addr >> 2) & (mpl - 1); /* On ne l'utilise pas en fait, mais bon */
      /* On appelle le n° de ligne set pluôt qu'index car c'est plus général */
      uint32_t set    = (insn_addr >> (offset_b + 2)) & ((1 << sets_b) - 1); 
      uint32_t tag    = (insn_addr >> (sets_b + offset_b + 2)) & ((1 << tag_b) - 1); 

      int32_t hit_v = -1; // Voie qui fait hit, il y en a au plus une
      for (int i = 0; i < v; i++)
         if (cache[i][set].v == 1 && cache[i][set].tag == tag) {
            hit_v = i;
            break;
         }

      if (hit_v >= 0) {
         /* Hit, rien à faire */
         hits[hit_v]++;
         if (verbose)
            printf("Hit:  ");
         for (int i = 0; i < v; i++)
            if (i == hit_v)
               cache[i][set].age = 0;
            else
               cache[i][set].age++; 
      } else {
         /* Miss, mise à jour des méta-données */
         /*\
          * On cherche la première voie invalide, mais normalement les voies sont 
          * rapidement toutes utilisées.
          * Ca fait néanmoins une différence pour les traces d'invaders, ...
         \*/
         hit_v = -1;
         for (int i = 0; i < v; i++)
            if (cache[i][set].v == 0) {
               hit_v = i;
               break;
            }
         if (hit_v == -1) {
            hit_v = random() & ((1 << ways_b) - 1); /* Politique de remplacement aléatoire */
            int old = -1; /* Politique de remplacement LRU très très crade, ... */
            for (int i = 0; i < v; i++)
               if (cache[i][set].age > old) {
                  hit_v = i;
                  old = cache[i][set].age;
               }
         }
         cache[hit_v][set].v   = 1;
         cache[hit_v][set].tag = tag;
         for (int i = 0; i < v; i++)
            if (i == hit_v)
               cache[i][set].age = 0;
            else
               cache[i][set].age++; 
         if (verbose)
            printf("Miss: ");
      }
      if (verbose) {
         printf("addr 0x%08x set 0x%08x, way 0x%08x, tag 0x%08x", insn_addr, set, hit_v, tag);
         if (v > 1)
            for (int i = 0; i < v; i++)
               printf(", [v%d:%d]", i, cache[i][set].age);
         printf("\n");
      }

      if (insns && insns % 1000 == 0) {
         uint32_t hit = 0;
         for (int i = 0; i < v; i++)
            hit += hits[i];

         printf("k-insns: %d, taux de miss: %f\n", insns/1000, 100 * (1.0 - (float)hit/(float)insns));
      }
   }

   /* Résumé */
   uint32_t hit = 0;
   for (int i = 0; i < v; i++)
      hit += hits[i];

   printf("insns: %d, taux de miss: %f\n", insns, 100 * (1.0 - (float)hit/(float)insns));

   return 0;
}
